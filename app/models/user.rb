class User < ActiveRecord::Base
  belongs_to :subject
  validates_presence_of :email, :message, :start_date, :end_date
end
