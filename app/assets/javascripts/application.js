// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require bootstrap-sprockets
//= require bootstrap-datepicker
//= require jquery.validate
//= require jquery.validate.additional-methods
//= require_tree .

$(document).ready(function() {
	// $('#user_start_date').datepicker();
	// $('#user_end_date').datepicker();

	// $("#user_start_date").datepicker({
	// numberOfMonths : 1,
	// minDate: 0,
	// startDate: "Biginning of time",
	// autoclose: true,
	// format: 'dd-mm-yyyy',
	// onSelect: function (selected) {
	// var dt = new Date(selected);
	// dt.setDate(dt.getDate() + 14);
	// $("#user_end_date").datepicker("option", "minDate", dt);
	// }
	// });
	// $("#user_end_date").datepicker({
	// numberOfMonths : 1,
	// onSelect : function(selected) {
	// var dt = new Date(selected);
	// dt.setDate(dt.getDate() - 14);
	// $("#user_start_date").datepicker("option", "maxDate", dt);
	// }
	// });

	validate_form();
	
	$("#txtFrom").datepicker({
		startDate : "Biginning of time",
		autoclose : true,
		onSelect : function(selected) {
			var dt = new Date(selected);
			dt.setDate(dt.getDate() + 14);
			$("#txtTo").datepicker("option", "minDate", dt);
		}
	});
	$("#txtTo").datepicker({
		autoclose : true,
		onSelect : function(selected) {
			var dt = new Date(selected);
			dt.setDate(dt.getDate() - 5);
			$("#txtFrom").datepicker("option", "maxDate", dt);
		}
	});
});

function validate_form(){
	$('#new_user').validate({
		rules:{
			'email': {
				required: true
			}
		},
		messages:{
			'email': {
				required: "please enter email address"
			}
		}
		
	});	
}

