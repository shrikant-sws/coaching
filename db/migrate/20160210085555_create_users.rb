class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email
      t.string :message
      t.date :start_date
      t.date :start_date
      t.references :subject, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
